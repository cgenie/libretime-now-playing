[%raw "require('isomorphic-fetch')"];

open Express;

[@bs.val] external apiKey : string = "process.env.API_KEY";
[@bs.val] external host : string = "process.env.HOST";

Printf.printf("API_KEY: %s, HOST: %s\n", apiKey, host);

exception NotImplemented(string);

/* return the string value for [key], None if the key is not in [dict]
   TODO once BOption.map is released */
let getDictString = (dict, key) =>
  switch (Js.Dict.get(dict, key)) {
  | Some(json) => Js.Json.decodeString(json)
  | _ => None
  };

let hostUrl = (host, apiKey) => host ++ "/api/live-info-v2/format/json/api_key/" ++ apiKey;
let apiUrl = (host, apiKey, method) => host ++ "/api/" ++ method ++ "/format/json/api_key/" ++ apiKey;

let app = express();

App.get(app, ~path="/index.html") @@
  Middleware.from(
    (_next, _) =>
    {|
     <!DOCTYPE html>
     <html lang="en">
      <head>
        <meta charset="UTF-8">
        <title>ReasonReact Examples</title>
      </head>
      <body>
        Libretime now playing:
        <div id="libretime-now-playing"></div>

        <script src="/static/Index.js"></script>
      </body>
     </html>
    |} |>
    Response.sendString
  );

App.useOnPath(app, ~path="/static", {
  let options = Static.defaultOptions();
  Static.make("../build", options) |> Static.asMiddleware;
});

App.get(app, ~path="/:method") @@
  PromiseMiddleware.from(
    (_next, req, res) => {
      let method = switch (getDictString(Request.params(req), "method")) {
        | Some(m) => m
        | None => raise(NotImplemented("method"))
      };
      let url = apiUrl(host, apiKey, method);
      Printf.printf("Fetching url: %s\n", url);
      Js.Promise.(
        Fetch.fetch(url)
        |> then_(Fetch.Response.json)
        |> then_(json => res |> Response.sendJson(json) |> resolve)
      );
      //res |> Response.sendString("s") |> Js.Promise.resolve;
  });

let server = App.listen(app, ~port=9000, ())

