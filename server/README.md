# Server for the `libretime-now-playing` widget

# Build
```
yarn
```

# Build + Watch

```
yarn start
```

# Running

```
node src/Server.bs.js
```
