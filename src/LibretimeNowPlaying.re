open Live_info;

type state = {
  host: string,
  liveInfo: ref(option(Live_info_v2.t)),
  tickInterval: ref(int),
  timerId: ref(option(Js.Global.intervalId)),
};

/* Action declaration */
type action =
  Tick
  | LiveInfoFetched(Live_info_v2.t);

let hostUrl = (s) => s.host ++ "/live-info-v2";

/* Component template declaration.
   Needs to be **after** state and action declarations! */
let component = ReasonReact.reducerComponent("LibretimeNowPlaying");

let make = (~host, ~tickInterval=10000, _children) => {
  ...component,

  initialState: () => {host: host, liveInfo: ref(None), tickInterval: ref(tickInterval), timerId: ref(None)},

  /* State transitions */
  reducer: (action, state) =>
    switch (action) {
    | Tick =>
      ReasonReact.UpdateWithSideEffects(
        state,
        (self => {
          Js.Promise.(
            Fetch.fetch(hostUrl(state))
            |> then_(Fetch.Response.json)
            |> then_(json =>
                    json
                    |> Live_info.Live_info_v2.decode
                    |> resolve)
            |> then_(decoded => resolve(self.send(LiveInfoFetched(decoded))))
            |> ignore
          )
        })
      )
    | LiveInfoFetched(li) => ReasonReact.Update({...state, liveInfo: ref(Some(li))})
    },

  didMount: self => {
    self.state.timerId :=
      Some(Js.Global.setInterval(() => self.send(Tick), self.state.tickInterval^));
    // Also set a Tick on start
    let _ = self.send(Tick);
  },

  render: self => {
    let (currentShow, currentTrack) = switch (self.state.liveInfo^) {
      | None => ("", "")
      | Some(li) => (li.shows.current.name, li.tracks.current.metadata.track_title)
    };

    <div className="libretime-now-playing-widget">
      <div className="current-show">(ReasonReact.string(currentShow))</div>
      <div className="current-track">(ReasonReact.string(currentTrack))</div>
    </div>
  },
};
