/* Describes the format at /live-info-v2/ */

module Live_info_shows_current = {
  type t = {
    starts: string,
    ends: string,
    name: string,
  };

  let decode = json =>
    Json.Decode.{
      starts: json |> field("starts", string),
      ends: json |> field("ends", string),
      name: json |> field("name", string),
    };
};

module Live_info_shows = {
  type t = {
    current: Live_info_shows_current.t,
  };

  let decode = json =>
    Json.Decode.{
      current: json |> field("current", Live_info_shows_current.decode),
    };
};

module Live_info_tracks_metadata = {
  type t = {
    artist_name: string,
    track_title: string,
  };

  let decode = json =>
    Json.Decode.{
      artist_name: json |> field("artist_name", withDefault("", string)),
      track_title: json |> field("track_title", withDefault("", string)),
    };
};

module Live_info_tracks_current = {
  type t = {
    starts: string,
    ends: string,
    name: string,
    metadata: Live_info_tracks_metadata.t,
  };

  let decode = json =>
    Json.Decode.{
      starts: json |> field("starts", string),
      ends: json |> field("ends", string),
      name: json |> field("name", string),
      metadata: json |> field("metadata", Live_info_tracks_metadata.decode),
    };
};

module Live_info_tracks_next = {
  type t = {
    starts: string,
    ends: string,
    name: string,
    metadata: Live_info_tracks_metadata.t,
  };

  let decode = json =>
    Json.Decode.{
      starts: json |> field("starts", string),
      ends: json |> field("ends", string),
      name: json |> field("name", string),
      metadata: json |> field("metadata", Live_info_tracks_metadata.decode),
    };
};

module Live_info_tracks = {
  type t = {
    current: Live_info_tracks_current.t,
    next: Live_info_tracks_next.t,
  };

  let decode = json =>
    Json.Decode.{
      current: json |> field("current", Live_info_tracks_current.decode),
      next: json |> field("next", Live_info_tracks_next.decode),
    };
};

module Live_info_v2 = {
  type t = {
    shows: Live_info_shows.t,
    tracks: Live_info_tracks.t,
  };

  let decode = json =>
    Json.Decode.{
      shows: json |> field("shows", Live_info_shows.decode),
      tracks: json |> field("tracks", Live_info_tracks.decode),
    };
};
